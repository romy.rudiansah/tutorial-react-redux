import React, { Component } from 'react'
import TableComponent from '../components/TableComponent'
import { connect } from 'react-redux'
import { getUsersList, deleteDataUser } from '../actions/userAction'

class HomeController extends Component {

  componentDidMount = () => {
    this.props.dispatch(getUsersList())
    this.props.dispatch(deleteDataUser());
  }

  render() {
    return (
      <TableComponent />
    )
  }
}

export default connect()(HomeController)