import React, { Component } from 'react'
import { Container } from 'reactstrap'
import BackComponent from '../components/BackComponent'
import { connect } from 'react-redux'
import { getUserDetail } from '../actions/userAction'
import DetailUserComponent from '../components/DetailUserComponent'

class DetailUserController extends Component {
  componentDidMount() {
    this.props.dispatch(getUserDetail(this.props.id))
  }
  render() {
    return (
      <Container>
        <BackComponent />
        <h2>Detail User: </h2>
        <br />
        <DetailUserComponent />
      </Container>
    )
  }
}

export default connect()(DetailUserController)