import React from 'react'
import BootstrapTable from 'react-bootstrap-table-next';
import { Button, Container, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfo, faEdit, faTrash, faPlus } from "@fortawesome/free-solid-svg-icons";
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { Spinner } from 'reactstrap';
import swal from "sweetalert";
import { deleteUser } from '../actions/userAction';

const { SearchBar } = Search;

const handleClick = (dispatch, id) => {
  swal({
    title: "Are you sure?",
    // text: "Once deleted, you will not be able to recover this imaginary file!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      dispatch(deleteUser(id));
      swal("Data has been deleted!", {
        icon: "success",
      });
    }
  });
}

const defaultSorted = [{
  dataField: 'id',
  order: 'asc'
}]

const mapStateToProps = (state) => {
  return {
    getUsersList: state.users.getUsersList,
    errorUsersList: state.users.errorUsersList
  }
}

const TableComponent = (props) => {
  const columns = [
    {
      dataField: 'id',
      text: 'ID',
      sort: true,
      headerStyle: () => {
        return { textAlign: "center", width: "4%" }
      },
      style: () => {
        return { textAlign: "center" }
      }
    },
    {
      dataField: 'nama',
      text: 'Nama',
      sort: true
    },
    {
      dataField: 'alamat',
      text: 'Alamat'
    },
    {
      dataField: 'umur',
      text: 'Umur',
      sort: true
    },
    {
      dataField: 'nohp',
      text: 'No. Hp'
    },
    {
      dataField: 'link',
      text: 'Aksi',
      headerStyle: () => {
        return { textAlign: "center", width: "23.6%" }
      },
      formatter: (rowContent, row) => {
        return (
          <div>
            <Link to={'/users/detail/'+row.id}>
              <Button color="dark" className="m-2 btn-dark">
                <FontAwesomeIcon icon={faInfo} /> Detail
              </Button>
            </Link>
            <Link to={'/users/edit/'+row.id}>
              <Button color="dark" className="m-2 btn-dark">
                <FontAwesomeIcon icon={faEdit} /> Edit
              </Button>
            </Link>
            <Button color="dark" className="m-2 btn-dark" onClick={() => handleClick(props.dispatch, row.id)}>
              <FontAwesomeIcon icon={faTrash} /> Delete
            </Button>
          </div>
        )
      }
    }
  ];
  return (
    <Container className="mt-5 mb-5">
      { (props.getUsersList) ? (
        <ToolkitProvider
          bootstrap4 keyField='id' data={ props.getUsersList } columns={ columns } defaultSort={ defaultSorted }
          search
        >
          {
            props => (
              <div>
                <Row>
                  <Col>
                    <Link to={'users/create'}>
                      <Button color="dark" className="btn-dark">
                        <FontAwesomeIcon icon={faPlus} /> Create User
                      </Button>
                    </Link>
                  </Col>
                  <Col>
                    <div style={{
                        float: 'right'
                      }}>
                      <SearchBar { ...props.searchProps }/>
                      <br />
                      <br />
                    </div>
                  </Col>
                </Row>
                <BootstrapTable
                  { ...props.baseProps }
                  pagination={ paginationFactory() } 
                />
              </div>
            )
          }
        </ToolkitProvider> )
        : 
        (
          <div className='text-center'>
            { props.errorUsersList ? <h4>{props.errorUsersList}</h4> : <Spinner color='dark' /> }
          </div>
        )
        
      }
    </Container>
  )
}

export default connect(mapStateToProps, null)(TableComponent)
