import React from 'react'
import { Button, Col, Row } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'

const BackComponent = () => {
  return (
    <Row className='mt-5 mb-3'>
      <Col>
        <Link to={'/'}>
          <Button color="dark" className="btn-dark">
            <FontAwesomeIcon icon={faArrowLeft} /> Back
          </Button>
        </Link>
      </Col>
    </Row>
  )
}

export default BackComponent
