import React from "react";
import { Container } from "react-bootstrap";
import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  Button,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfo } from "@fortawesome/free-solid-svg-icons";
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
  return {
    title: state.users.title
  }
}

const CardComponent = (props) => {
  return (
    <div
      className="p-5"
      style={{
        backgroundColor: "#f5f5f5",
        borderColor: "transparent",
      }}
    >
      <Container>
        <Card
          style={{
            backgroundColor: "transparent",
            borderColor: "transparent",
            padding: 0,
          }}
        >
          <CardBody
            style={{
              padding: 0,
            }}
          >
            <CardTitle tag="h2">{props.title}</CardTitle>
            <CardText>
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </CardText>
            <Button>
              <FontAwesomeIcon
                icon={faInfo}
                style={{
                  marginRight: "10px",
                }}
              />
              Learn More
            </Button>
          </CardBody>
        </Card>
      </Container>
    </div>
  );
};

export default connect(mapStateToProps, null)(CardComponent);
