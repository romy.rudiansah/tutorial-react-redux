import React from "react";
import {
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink,
  NavbarText,
} from "reactstrap";

const NavbarComponent = (props) => {
  return (
    <Navbar color="dark" expand="md" dark container>
      <NavbarBrand href="/">FE - Xmple CRUD</NavbarBrand>
      <NavbarToggler onClick={function noRefCheck() {}} />
      <Collapse navbar>
        <Nav className="me-auto" navbar>
          <NavItem>
            <NavLink href="/components/">Home</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="https://github.com/reactstrap/reactstrap">
              About Us
            </NavLink>
          </NavItem>
        </Nav>
        <NavbarText>Admin</NavbarText>
      </Collapse>
    </Navbar>
  );
};

export default NavbarComponent;
