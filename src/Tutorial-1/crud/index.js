import React, { Component } from 'react'
import NavbarComponent from './NavbarComponent'
import Formulir from './Formulir'
import Tabel from './Tabel'

export default class Crud extends Component {
  
  constructor(props) {
    super(props)
  
    this.state = {
       dataForm : [],
       id : "",
       name: "",
       email : "",
       message : "",
    }
  }
  
  handleChange = (event) => {
    this.setState({
      [event.target.name] : event.target.value
    })
  }

  handleSubmit = (event) => {
    event.preventDefault();
     
    if(this.state.id === "") {
      this.setState({
        dataForm: [
          ...this.state.dataForm,
          {
            id: this.state.dataForm.length+1,
            name: this.state.name,
            email: this.state.email,
            message: this.state.message
          }
        ]
      })
  
    }else{
      const dataFormNotSelected = this.state.dataForm
        .filter((dataForm) => dataForm.id !== this.state.id)
        .map((filterDataForm) => {
          return filterDataForm
        })

        this.setState({
          dataForm: [
            ...dataFormNotSelected,
            {
              id: this.state.dataForm.length+1,
              name: this.state.name,
              email: this.state.email,
              message: this.state.message
            }
          ]
        })
    }
  
    this.setState({
      id : "",
      name: "",
      email : "",
      message : "",
    })

  }

  editData = (id) => {
    const dataFormSelected = this.state.dataForm
      .filter((dataForm) => dataForm.id === id)
      .map((filterDataForm) => {
        return filterDataForm
      })

    this.setState({
      id : dataFormSelected[0].id,
      name: dataFormSelected[0].name,
      email : dataFormSelected[0].email,
      message : dataFormSelected[0].message,
    })
  }

  hapusData = (id) => {
    console.log(id)
    const dataFormNew = this.state.dataForm
        .filter((dataForm) => dataForm.id !== id)
        .map((filterDataForm) => {
          return filterDataForm
        })

      this.setState({
        dataForm: dataFormNew
      })
  }
  
  render() {
    console.log(this.state.dataForm);
    return (
      <div>
        <NavbarComponent />
        <div className="container">
          <br />
          <br />
          <Formulir {...this.state} handleChange={this.handleChange} handleSubmit={this.handleSubmit} />
          <Tabel dataForm={this.state.dataForm} editData={this.editData} hapusData={this.hapusData}/>
        </div>
      </div>
    )
  }
}
