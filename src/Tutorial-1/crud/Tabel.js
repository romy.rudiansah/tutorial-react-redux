import React from 'react'
import { Table } from 'react-bootstrap'

const Tabel = ({dataForm, editData, hapusData}) => {
  return (
    <Table striped bordered hover className='mt-3'>
      <thead>
        <tr>
          <th>#</th>
          <th>Full Name</th>
          <th>Email</th>
          <th>Message</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {dataForm.map((dataForm, index) => {
          return (
            <tr key={index}>
              <td>{index+1}</td>
              <td>{dataForm.name}</td>
              <td>{dataForm.email}</td>
              <td>{dataForm.message}</td>
              <td>
                <button className="btn btn-warning m-2" onClick={() => editData(dataForm.id)}>Edit</button>
                <button className="btn btn-danger m-2" onClick={() => hapusData(dataForm.id)}>Hapus</button>
              </td>
            </tr>
          )
        })}
      </tbody>
    </Table>
  )
}

export default Tabel
