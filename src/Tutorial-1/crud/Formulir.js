import React from 'react'
import { Form, Row, Col, Button } from 'react-bootstrap'

const Formulir = ({name, email, message, handleChange, handleSubmit, id}) => {
  return (
    <div className='mt-3 col-6 offset-3'>
      <Row>
        <Col>
          <h4>{ !id ? "Tambah Data" : "Edit Data" }</h4>
          <br/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="Name">
              <Form.Label>Full Name</Form.Label>
              <Form.Control 
                type="text"
                name="name"
                placeholder="Enter Name"
                value={name}
                onChange={(event) => handleChange(event)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="Email">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                name="email"
                placeholder="Enter Email"
                value={email}
                onChange={(event) => handleChange(event)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="Message">
              <Form.Label>Message</Form.Label>
              <Form.Control
                as="textarea"
                name="message"
                rows={3}
                value={message}
                onChange={(event) => handleChange(event)}
              />
            </Form.Group>

            <Button variant="primary" type="submit" name="submit">
              Submit
            </Button>
          </Form>
        </Col>
      </Row>
    </div>
  )
}

export default Formulir
