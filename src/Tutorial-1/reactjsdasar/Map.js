import React from 'react'

const makanan = [
  {
    nama: "Soto",
    harga: 12000
  },
  {
    nama: "Bakso",
    harga: 10000
  },
  {
    nama: "Mie Ayam",
    harga: 7000
  },
  {
    nama: "Nasi Goreng",
    harga: 15000
  }
]

// Reduce
const total_harga = makanan.reduce((total_harga, makanan) => {
  return total_harga+makanan.harga
}, 0);

const Maps = () => {
  return (
    <div>
      <h2>Map</h2>
      <div>
        {makanan.map((makanan, index) => (
          <p>{index + 1}. {makanan.nama} - Harga {makanan.harga}</p>
        ))}
      </div>
      <br/>
      <h2>Map Filter Harga Lebih Dari 11.000</h2>
      {makanan
        .filter((makanan) => (makanan.harga) >= 11000)
        .map((makanan, index) => (
          <p>{index+1}. {makanan.nama} - Harga : {makanan.harga}</p>
        ))
      }
      <br/>
      <h1>Total Harga : {total_harga}</h1>
    </div>
  )
}

export default Maps
