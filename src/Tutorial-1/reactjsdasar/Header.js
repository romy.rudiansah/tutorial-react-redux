import React, { Component } from 'react'

// Class Component
export default class Header extends Component {
  render() {
    return (
      <div>
        <h2>Header</h2>
      </div>
    )
  }
}

// Function Component
// import React from 'react'

// export default function Header() {
//   return (
//     <div>
//       <h2>Header</h2>
//     </div>
//   )
// }

// Arrow Function
// const Header = () => {
//   return (
//     <div>
//       <h2>Header</h2>
//     </div>   
//   )
// }

// export default Header