import React from 'react'

// Const

const harga = 10000;
// harga cant re-declare

// Var
var harga = 10000;
if (harga) {
  harga = 30000;
}
// global


let harga = 10000;
if (harga) {
  let harga = 30000;
  // let cant re-declare on block scope
}

const Variable = () => {
  return (
    <div>
      Harga : {harga}
    </div>
  )
}

export default Variable
