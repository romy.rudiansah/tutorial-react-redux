import React, { Component } from 'react'
import { Route, Routes, useParams } from "react-router-dom";

import NavbarComponent from './components/NavbarComponent';
import CardComponent from './components/CardComponent';
import HomeController from './controllers/HomeController';
import EditUserController from './controllers/EditUserController';
import DetailUserController from './controllers/DetailUserController';
import CreateUserController from './controllers/CreateUserController';

function DetailUser() {
  let params = useParams();
  return (
    <DetailUserController id={params.userId}/>
  )
}

function EditUser() {
  let params = useParams();
  return (
    <EditUserController id={params.userId}/>
  )
}

export default class App extends Component {
  render() {
    return (
      <div>
        <NavbarComponent />
        <CardComponent/>
        
        <Routes>
          <Route exact path='/' element={<HomeController />} />
          <Route path='/home' element={<HomeController />} />
          <Route path="users">
            <Route exact path="create" element={<CreateUserController />} />
            <Route exact path={"edit/:userId"} element={<EditUser />} />
            <Route exact path={"detail/:userId"} element={<DetailUser />} />
          </Route>
        </Routes>
      </div>
    )
  }
}